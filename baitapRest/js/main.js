
let trungBinh = (...nums) => 
{
    let sum = 0;  
    for(const num of nums)
    {
        sum +=num;
        n= nums.length;
    }
    return (sum/n).toFixed(2);
}

function tinhDTBKhoi1(){
    var diemToan = document.getElementById("inpToan").value*1;
    var diemLy = document.getElementById("inpLy").value*1;
    var diemHoa = document.getElementById("inpHoa").value*1;
    let score = trungBinh(diemToan, diemLy, diemHoa);
    document.getElementById("tbKhoi1").innerText = `${score}`;
}

function tinhDTBKhoi2(){
    var diemVan = document.getElementById("inpVan").value*1;
    var diemSu = document.getElementById("inpSu").value*1;
    var diemDia = document.getElementById("inpDia").value*1;
    var diemAnh = document.getElementById("inpEnglish").value*1;
    let score = trungBinh(diemVan, diemSu, diemDia, diemAnh);
    document.getElementById("tbKhoi2").innerText = `${score}`;
}


