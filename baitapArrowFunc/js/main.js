const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "celadon", "saffron", "fuschia", "cinnabar"];
  
let colorContainer = document.getElementById("colorContainer");

let arr = [];
  
const changeColor = (event, color) => {
let house = document.getElementById("house");
house.classList.add(color);

const activeElenments= document.querySelector(".active");
if(activeElenments!=null){
    activeElenments.classList.remove("active");

}
event.target.classList.add("active");
};
  
for (let index = 0; index < colorList.length; index++) {
let button = document.createElement("button");
button.className = colorList[index];
const list = button.classList;
list.add("color-button");
button.addEventListener(
    "click",
    (event) => changeColor(event,colorList[index]),
    false
);
arr.push(button);
}
colorContainer.append(...arr);